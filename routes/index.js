var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res) {
  res.render('index', { title: 'Home' });
});
/*GET katalog page*/
router.get('/katalog', function(req, res) {
  res.render('katalog', { title: 'Katalog' });
});
/*GET jadwal page*/
router.get('/jadwal', function(req, res) {
  res.render('jadwal', { title: 'Jadwal Lelang' });
});
/*GET syarat page*/
router.get('/syarat', function(req, res) {
  res.render('syarat', { title: 'Syarat dan Keterangan' });
});
/*GET about page*/
router.get('/about', function(req, res) {
  res.render('about', { title: 'Tentang Awi' });
});
/*GET kategori page*/
router.get('/kategori', function(req, res) {
  res.render('kategori', { title: 'Kategori' });
});

router.get('/singlepage', function(req, res) {
  res.render('singlepage', { title: 'Kategori' });
});

router.get('/login', function(req, res) {
  res.render('login-popup', { title: 'Kategori' });
});

module.exports = router;
