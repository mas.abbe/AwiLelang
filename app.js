var express = require('express');
var path = require('path');
var favicon = require('static-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');

//==========controller===========//
var routes = require('./routes/index');
var users = require('./routes/users');

var admin = require ('./admin/app');
var model = require ('./db-mysql');

var app = express(); //main app
// ==============================view engine setup==================================================//
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// ================================module use setup=================================================//
app.use(favicon());
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}
    ));
admin.use(session({
    secret: 'AwiLelang',
    resave: false,
    saveUninitialized: true,
    cookie: { secure: true }
}
));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

/*=========page routes======*/
app.use('/', routes);
app.use('/katalog', routes);
app.use('/kategori', routes);
app.use('/jadwal',routes);
app.use('/syarat',routes);
app.use('/about',routes);
app.use('/users', users);
app.use('/admin', admin);
app.use('singlepage', routes);
app.use('/signup',routes)
// ================================catch 404 and forwarding to error handler============================//
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

/*=====error handlers========*/

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

module.exports = app;
