var info = document.getElementById('popup-info');
var btn  = document.getElementById("btn-info");
var span = document.getElementsByClassName("close")[0];


btn.onclick = function() {
    info.style.display = "block";
}

span.onclick = function() {
    info.style.display = "none";
}

window.onclick = function(event) {
    if (event.target == info) {
        info.style.display = "none";
    }
}