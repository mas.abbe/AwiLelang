var express = require('express');
var path = require('path');
var favicon = require('static-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');


//Controllers
var posts = require('./routes/posts');
var routes= require('./routes/routes');
var users = require('./routes/users');
var model = require ('../db-mysql');

var admin = express(); // the sub app

admin.use(session(
	{
    secret: 'AwiLelang',
    resave: false,
    saveUninitialized: true,
    cookie: {
    			secure: true,
    			maxAge: 60000
    		}
	}
));

admin.use(bodyParser.json());
admin.use(bodyParser.urlencoded({extended: true}));


// ==============================admin view engine setup==================================================//

admin.set('views', path.join(__dirname, 'views'));
admin.set('view engine', 'ejs');

admin.get('/logout', users);
admin.get('/login', routes);
admin.post('/login', users);


admin.use('/',routes);
admin.use('/main', routes)
admin.use('/barang',routes);
admin.use('/tambah-barang',routes);

admin.use('/user-admin',routes);
admin.use('/jadwal-lelang',routes);

admin.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});
// production error handler
// no stacktraces leaked to user
admin.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

module.exports = admin;
