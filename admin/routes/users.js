var express = require('express');
var session = require('express-session');
var crypto = require('crypto-js');
var bodyParser = require('body-parser');

var model = require('../../db-mysql');

var users = express.Router();

users.use(bodyParser.json());
users.use(bodyParser.urlencoded({extended: true}));

users.get('/logout', function(req, res){
   req.session.destroy(function(){
      console.log("user logged out.")
   });
   res.redirect('/login');
});

// accept POST request on the admin posts index via posts/
users.post('/login', function (req, res) {
console.log('post login');
 	if (!req.session.admin) {
		var data = {
			text: "SELECT * FROM USER WHERE ? AND ?",
			placeholder: [{username: req.body.username}, {password: /*crypto.MD5(password1).toString()*/ req.body.password}],
		}
		console.log('post login, '+req.bodyParser.username);
		model.mySQLQuery(data).then(function (result){
			console.log(result);
			if(result.length){
				req.session.admin=req.body.username;
				res.send('success');
				console.log('success login admin, session admin ke dashboard');
			}else{
				res.send('failure');
				console.log('gagal login admin');
			}
		}).catch(function(error){
			console.log('Error while executing SQL Query', err);
		});
	}else{
		res.redirect('/login');
	}
});

module.exports = users;
