var express = require('express');
var session = require('express-session');
var crypto = require('crypto-js');
var bodyParser = require('body-parser');

var model = require('../../db-mysql');

var router = express.Router();

router.get('/login', function(req, res){
	res.render('login', {title: "Login"});
});
router.get('/', function(req, res){
	res.render('dashboard', {title: "Dashboard"});
});

router.get('/barang', function(req, res){
	res.render('barang', {title: "Stok Barang"});
});
router.get('/tambah-barang', function(req, res){
	res.render('tambah-barang', {title: "tambah-barang"});
});
module.exports = router;
